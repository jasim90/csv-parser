from django import forms

from web.models import FileManager


class CSVUploadForm(forms.ModelForm):
    csv = forms.FileField(label='CSV', required=True)

    class Meta:
        model = FileManager
        fields = ['csv']

    def clean_csv(self):
        csv_file = self.files.get('csv')
        if csv_file.content_type != 'text/csv':
            raise forms.ValidationError("Invalid File")
        return csv_file
