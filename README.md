# CSV Parser (Python 3.5.2)
This is a sample application for parsing a CSV file


The application first upload file and it will be parsed via celery


Celery is configured with RabbitMQ

Command for celery starting worker: celery -A csv_parser worker -l info


Note: For development I have used only the sqlite db