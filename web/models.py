from django.db import models

# Create your models here.


class Inventory(models.Model):
    inventory_key = models.IntegerField()
    catalog_no = models.CharField(max_length=20)
    catalog_color = models.CharField(max_length=20)
    size = models.CharField(max_length=5)
    quantity = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    catalog_price = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    is_on_sale = models.BooleanField()
    created_on = models.DateTimeField(auto_now_add=True)


class FileManager(models.Model):
    choices = ((1, 'Submitted'), (2, 'Parsing'), (3, 'Success'), (4, 'Error'))
    csv = models.FileField()
    status = models.IntegerField(default=1, choices=choices)
    created_on = models.DateTimeField(auto_now_add=True)
