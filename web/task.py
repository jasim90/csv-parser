import csv
from datetime import datetime

from celery import shared_task

from web.models import Inventory, FileManager


@shared_task
def parse_file(data):
    '''
    For parsing the file
    '''
    file_id = data.get('file_id')
    file_manager = FileManager.objects.get(id=file_id)
    file_manager.status = 2
    file_manager.save()
    reader = csv.DictReader(open(file_manager.csv.path))
    data = []
    for row in reader:
        sale_end_datetime = datetime.strptime(row.get('sale_end_datetime'), '%m-%d-%Y') if row.get('sale_end_datetime') else None
        if sale_end_datetime and sale_end_datetime > datetime.now():
            catalog_price = row.get('case_sale_price')
            is_on_sale = True
        else:
            catalog_price = row.get('case_price')
            is_on_sale = False
        data.append(Inventory(
            inventory_key=row.get('inventory_key'),
            catalog_no=row.get('catalog_no'),
            catalog_color=row.get('catalog_color'),
            size=row.get('size'),
            quantity=row.get('quantity'),
            catalog_price=catalog_price,
            is_on_sale=is_on_sale
        ))
    # Bulk Create
    try:
        Inventory.objects.bulk_create(data)
        file_manager.status = 3
    except Exception as e:
        file_manager.status = 4
    file_manager.save()
