
from django.contrib import messages
from django.shortcuts import render, redirect

from django.views import View
from django.views.generic import ListView

from web.forms import CSVUploadForm
from web.models import FileManager
from web.task import parse_file


class CSVUpload(View):
    def get(self, request):
        return render(request, 'upload.html', {'form': CSVUploadForm()})

    def post(self, request):
        form = CSVUploadForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.save()
            parse_file.delay({'file_id': data.id})
            messages.success(request, 'CSV uploaded successfully.')
            return redirect('csv_history')
        else:
            return render(request, 'upload.html', {'form': form})


class FileStatus(ListView):

    model = FileManager
    ordering = '-id'
